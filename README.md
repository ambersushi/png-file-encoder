## About
A simple bash script to encode any arbitrary file in the RGB pixel data of a PNG image.

This is not a steganography program; file data directly corresponds to RGB values.

PNG encoder encoded in a PNG:

![image](./examples/example.png)

## Requirements
imagemagick convert

## Usage
```bash
./pngencode.sh <input_file> <output_png_name>
```

There is necessarily a null-padded gap at the end of the image data if input file's data didn't fill to a perfect square.

File formats best encoded are those that can be read with arbitrary trailing zeros.

## License
MIT
