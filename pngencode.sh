#!/bin/bash

# Checking for file.
if [[ $1 == "" ]] || [[ $2 == "" ]]
then
	echo -e "Usage:\n\t$0 <file_to_encode_in_png> <output_png_name>\n\tThe .png extension will automatically be added."

	exit 1
fi

# Checking if output file already exists.
if [[ -e "$2.png" ]]
then
	echo "Output file \"$2.png\" already exists."

	exit 2
fi

# Creating temp ppm file name.
temp=$(uuidgen)

# Getting filesize of input file.
filesize=$(stat --printf="%s\n" $1)

# Finding next largest perfect square for image size that can hold input file.
size=0
while [ 1 ]
do
	# Checking if the current image size will hold the input file.
	if [[ $(($size * $size * 3)) -ge $filesize ]]
	then
		# Exiting loop if it will.
		break
	fi

	# Incrementing size if it won't.
	size=$(($size + 1))
done

# Adding the header to the output file.
echo -e "P6\n$size $size\n255\n" > /tmp/$temp.ppm

# Adding image data to output file.
cat $1 >> /tmp/$temp.ppm

# Getting the amount of padding needed for output file.
paddingLength=$(($(($size * $size * 3)) - $filesize))

# Padding file with zeros.
xxd -l $paddingLength -p /dev/zero >> /tmp/$temp.ppm

# Converting the temp ppm file to a png.
convert /tmp/$temp.ppm $2.png

# Removing temp file.
rm -f /tmp/$temp.ppm

exit 0
